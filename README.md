# Introducción a los algoritmos
##### Práctica calificada

## Instrucciones

* Haga un fork de este repositorio para dar solución a las preguntas planteadas.
* En los comandos a continuación, Ud. debe reemplazar `tu_usuario` por su propio
nombre de usuario, `tucorreo` por su propio correo del tipo `u20241xxxx` y
`tucodigo` por su propio código del tipo `20241xxxx`.
* Clone el fork a su computadora con el comando
`git clone https://gitlab.com/tu_usuario/202401_iala_pc2.git`
* En su computadora no olvide establecer su usuario y correo con los comandos
`git config --global user.name "tu_usuario"` y
`git config --global user.email "tucorreo@upc.edu.pe"`
* El repositorio ya incluye el correspondiente archivo `.gitignore`
* Cree su proyecto dentro de la carpeta del repositorio con el nombre
`tucodigo`
* Desarrolle y haga `git add .` cada vez que cree algun archivo nuevo y
`git commit -a -m "sus comentarios"` por lo menos una vez por cada pregunta.
* Al finalizar, en los últimos 10 minutos, haga `commit push`.
* Luego abra un pull request al repositorio original desde [Gitlab](gitlab.com)
mismo.
* Es extremadamente importante que siga al pie de la letra las instrucciones.
* Debe subir todos los archivos cpp y h a la actividad correspondiente en el
aula virtual.
* Obtendrá tres puntos adicionales si envia el **pull request** indicado. 


## Punteros

El método clásico para determinar si un número es primo o no, consiste en
determinar si el número es divisible exactamente por algún número entre 2 y la
raiz cuadrada del número original.

Por ejemplo para verificar si 19 es primo, hacemos el siguiente procedimiento:

| Divisor | Divisible? 
| -- | --
| 2       |  no
| 3       |  no
| 4       |  no
| 5       |  no
| 6       |  no

Por lo tanto 19 es primo.

### Pregunta 

(7 puntos)

Implemente una función que determine si un número es primo usando el
procedimiento descrito anteriormente. Verifique su procedimiento mostrando todos
primos entre los primeros N números naturales.

Para esta función debe debe utilizar únicamente punteros y solo que punteros.

## La criba de eratóstenes

La criba de Eratóstenes es un método más eficiente para encontrar los números
primos entre los primeros primeros N números de una colección (o arreglo). El
procedimiento consiste en llenar un arreglo con los números de 2 a N, estos
números se irán marcando como **primos**  o ~~no primos~~ durante el proceso.
Se debe tomar el primer número sin ~~marcar~~, dicho número es **primo**, luego
se marcarán todos los múltiplos de dicho número como ~~no primos~~. Se repite el
procedimiento hasta haber recorrido todos los números del arreglo.

| Número | 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20
| ------ |--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|-- 
|   2    | P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P 
|   3    |  | P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P 
|   4    |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   5    |  |  |  | P| P| P| P| P| P| P| P| P| P| P| P| P| P| P| P
|   6    |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   7    |  |  |  |  |  | P| P| P| P| P| P| P| P| P| P| P| P| P| P
|   8    |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   9    |  |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   10   |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   11   |  |  |  |  |  |  |  |  |  | P| P| P| P| P| P| P| P| P| P
|   12   |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   13   |  |  |  |  |  |  |  |  |  |  |  | P| P| P| P| P| P| P| P
|   14   |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   15   |  |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   16   |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   17   |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | P| P| P| P
|   18   |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP
|   19   |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | P| P
|   20   |NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP|NP

* P: Primo
* NP: No primo

(Criba de eratóstenes en Wikipedia)[https://es.wikipedia.org/wiki/Criba_de_Erat%C3%B3stenes]

### Pregunta

(8 puntos)

Implemente una función que reciba N como parámetro y retorne un arreglo dinámico
con la criba correspondiente, puede marcar a los no primos como -1.
Muestre todos los números primos entre los primeros N números naturales.


## Arreglos dinámicos

Tomando como base el código del ejemplo arreglo dinámico proporcionado, y visto
en clases, implemente una opción adicional que permita eliminar elementos.

### Eliminar por índice

(2 puntos)

Implemente una función que permita eliminar un elemento dado su índice, sin
necesidad de preservar el orden relativo de los elementos.

### Eliminar por índice 2.0

(3 puntos)

Implemente una función que permita eliminar un elemento dado su índice, pero
preservando el orden relativo de los elementos.

